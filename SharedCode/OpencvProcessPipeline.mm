//
//  CapturePipeLine.m
//  CameraPipeLine
//
//  Created by JiangZhiping on 15/10/23.
//  Copyright © 2015年 JiangZhiping. All rights reserved.
//

#import "OpencvProcessPipeline.h"
#import "CameraProcessInput.h"
#import "VideoProcessInput.h"
#import <CocoaCVUtilities/CocoaCVUtilities.h>
#if TARGET_OS_IPHONE
#import "CameraProcessInputWithSettings.h"
#endif
#import <CoreMedia/CoreMedia.h>
#import <CoreImage/CoreImage.h>

@interface OpencvProcessPipeline () <ProcessInputDelegate>

@property (strong, nonatomic, nullable) MTKImageView * imageView;

@end

@implementation OpencvProcessPipeline {
    
    AVAssetWriterInput* assetWriterInput;
    AVAssetWriterInputPixelBufferAdaptor* pixelBufferAdaptor;
    AVAssetWriter* assetWriter;
    
    CMTime lastFrameTimestamp;
    CMVideoDimensions currentVideoDimensions;
    
    NSURL *videoOutputURL;
    NSURL *imageOutputURL;
    
    bool takePictureToken;

    cve::TickTockTimer * chronoTimer;
}

#pragma mark - init methods

- (void)setPreviewRootView:(NSUIView *)previewRootView {
    _previewRootView = previewRootView;
    _imageView = [[MTKImageView alloc] init];
    [_previewRootView addSubview:_imageView];
}

- (instancetype) initWithVideoFilePath:(NSString *)filePath {
    self = [super init];
    if (self) {
        _processBridge = [OpenCVProcessingBridge getInstance];
        _processInput = [[VideoProcessInput alloc] initWithFilePath:filePath];
        _processInput.delegate = self;
        [NSObject letObject:self accessibleByName:@"OpenCVProcessPipeline"];
    }
    return self;
}

- (instancetype) initWithCameraPosition:(AVCaptureDevicePosition)initialCameraPosition {
    self = [super init];
    if (self) {
        _processBridge = [OpenCVProcessingBridge getInstance];
#if TARGET_OS_IPHONE
        _processInput = [[CameraProcessInputWithSettings alloc] initWithCameraPosition:initialCameraPosition];
#else
        _processInput = [[CameraProcessInput alloc] initWithCameraPosition:initialCameraPosition];
#endif
        chronoTimer = new cve::TickTockTimer("ProcessingBridge", 50);
        _processInput.delegate = self;
        [NSObject letObject:self accessibleByName:@"OpenCVProcessPipeline"];
    }
    return self;
}

#pragma mark - action methods

- (void)setPipelineRunning:(BOOL)pipelineRunning {
    if (_pipelineRunning == pipelineRunning) {
        return;
    }
    
    if (pipelineRunning) {
        [_processInput startRunning];
    } else {
        [_processInput stopRunning];
    }
    _pipelineRunning = pipelineRunning;
}

- (void) startRunning {
    self.pipelineRunning = YES;
}

- (void) stopRunning {
    self.pipelineRunning = NO;
}

- (void) startRecording {
    
    NSDictionary *videoCompressionSettings = @{AVVideoCodecKey: AVVideoCodecH264,
                                              AVVideoWidthKey: @(currentVideoDimensions.width),
                                              AVVideoHeightKey: @(currentVideoDimensions.height)};
    assetWriterInput = [[AVAssetWriterInput alloc] initWithMediaType:AVMediaTypeVideo outputSettings:videoCompressionSettings];
    assetWriterInput.expectsMediaDataInRealTime = YES;
    
    NSDictionary * adaptorSettings = @{(id)kCVPixelBufferPixelFormatTypeKey: @(kCVPixelFormatType_32BGRA),
                                      (id)kCVPixelBufferWidthKey: [NSNumber numberWithUnsignedInteger:currentVideoDimensions.width],
                                      (id)kCVPixelBufferHeightKey: [NSNumber numberWithUnsignedInteger:currentVideoDimensions.height]};
    
    pixelBufferAdaptor = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:assetWriterInput sourcePixelBufferAttributes:adaptorSettings];
    
    NSString * documentDirPath = [[NSFileManager defaultManager] documentsDirectory];
    
    NSString *temporaryFileName = [NSProcessInfo processInfo].globallyUniqueString;
    NSString * videoTemporaryPath = [documentDirPath stringByAppendingPathComponent:[temporaryFileName stringByAppendingPathExtension:@"mov"]];
    
    videoOutputURL = [NSURL fileURLWithPath:videoTemporaryPath];
    assetWriter = [AVAssetWriter assetWriterWithURL:videoOutputURL fileType:AVFileTypeQuickTimeMovie error:nil];
    
    if ([assetWriter canAddInput:assetWriterInput]) {
        [assetWriter addInput:assetWriterInput];
    }
    
    if (assetWriter.status != AVAssetWriterStatusWriting) {
        [assetWriter startWriting];
        [assetWriter startSessionAtSourceTime:lastFrameTimestamp];
    }
}

- (void) stopRecording {
    [assetWriter finishWritingWithCompletionHandler:^(void) {
    }];
    assetWriterInput = nil;
    pixelBufferAdaptor = nil;
    assetWriter = nil;
#if TARGET_OS_IPHONE
    [self writeToPhotoLibrary:videoOutputURL Type:@"video"];
#endif
}

- (void) takePicture {
    takePictureToken = YES;
}

- (void) takePictureTask:(CVPixelBufferRef)pb {
    cv::Mat rgba = [CIImage conversionCvMatFromCVPixelBuffer:pb];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentDirPath = paths[0];
    NSString *temporaryFileName = [NSProcessInfo processInfo].globallyUniqueString;
    NSString * imageTemporaryPath = [documentDirPath stringByAppendingPathComponent:[temporaryFileName stringByAppendingPathExtension:@"jpg"]];
    cv::imwrite(imageTemporaryPath.UTF8String, rgba);
    imageOutputURL = [NSURL fileURLWithPath:imageTemporaryPath];
    
#if TARGET_OS_IPHONE
    [self writeToPhotoLibrary:imageOutputURL Type:@"image"];
#endif
    rgba.release();
    takePictureToken = NO;
}
#if TARGET_OS_IPHONE
- (void) writeToPhotoLibrary:(NSURL *) assetURL Type:(NSString *)type {
    [PHPhotoLibrary requestAuthorization:^( PHAuthorizationStatus status ) {
        if (status == PHAuthorizationStatusAuthorized) {
            
            PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
            fetchOptions.predicate = [NSPredicate predicateWithFormat:@"title = %@",@"VADS"];
            PHAssetCollection * collection = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAny options:fetchOptions].firstObject;
            __block PHObjectPlaceholder * placeHolder = nil;
            if (!collection) {
                [[PHPhotoLibrary sharedPhotoLibrary] performChangesAndWait:^{
                    PHAssetCollectionChangeRequest * createAlbum = [ PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:@"VADS"];
                    placeHolder = [createAlbum placeholderForCreatedAssetCollection];
                } error:nil];
            }
            
            
            [[PHPhotoLibrary sharedPhotoLibrary] performChangesAndWait:^{
                PHAssetChangeRequest * asssetRequest;
                if ([type isEqualToString:@"video"]) {
                    asssetRequest = [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:videoOutputURL];
                } else {
                    asssetRequest = [PHAssetChangeRequest creationRequestForAssetFromImageAtFileURL:imageOutputURL];
                }
                placeHolder = [asssetRequest placeholderForCreatedAsset];
                PHFetchResult * photosAsset = [PHAsset fetchAssetsInAssetCollection:collection options:nil];
                PHAssetCollectionChangeRequest *albumChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:collection assets:photosAsset];
                [albumChangeRequest addAssets:@[placeHolder]];
            } error:nil];
        }
    }];
}
#endif

#pragma mark - AVCaptureVideoDataOutputSampleBufferDelegate

- (void) newPixelBufferDidReceive:(CVPixelBufferRef)aPixelBufferRef AtTime:(CMTime)time FrameCount:(NSUInteger) count {
    currentVideoDimensions.width = CVPixelBufferGetWidth(aPixelBufferRef);
    currentVideoDimensions.height = CVPixelBufferGetHeight(aPixelBufferRef);
//    NSLog(@"video frame interval: %f", CMTimeGetSeconds(time) - CMTimeGetSeconds(lastFrameTimestamp));
    lastFrameTimestamp = time;
    
    chronoTimer->tick();
    CIImage * processResult = [self.processBridge process:aPixelBufferRef];
    chronoTimer->tock();
    if (self.previewRootView) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imageView.image = processResult;
        });
    }
    
    if ((assetWriter && assetWriterInput.readyForMoreMediaData) || takePictureToken) {
        CVPixelBufferRef pb = NULL;
        pb = processResult.cvPixelBuffer;

        if (takePictureToken) {
            [self takePictureTask:pb];
            CVPixelBufferRelease(pb);
            return;
        }
        
        [pixelBufferAdaptor appendPixelBuffer:pb withPresentationTime:lastFrameTimestamp];
        
        CVPixelBufferRelease(pb);
        
    }
    
}

@end
