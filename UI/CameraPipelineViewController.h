//
//  CameraPipelineViewController.h
//  OpenCVCameraPipeline
//
//  Created by JiangZhiping on 16/5/6.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CocoaCVUtilities/CocoaCVUtilities.h>
#import "OpencvProcessPipeline.h"
#import "CameraProcessInputWithSettings.h"

@interface CameraPipelineViewController : NSUIViewController

@property (nonatomic) OpencvProcessPipeline * pipeline;

@property (nonatomic, getter = isRecording) bool recording;

@property (nonatomic, getter = isDoneButtonShown) bool showDoneButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

- (IBAction)takePicture:(UIButton *)sender;
- (IBAction)changeCameraDirection:(UIButton *)sender;
- (IBAction)recordOrStopVideo:(UIButton *)sender;
- (IBAction)done:(UIButton *)sender;

- (instancetype) initCameraPipelineViewController;

@end
