//
//  OpenCVProcessingBridge.m
//  CameraPipeLine
//
//  Created by JiangZhiping on 15/11/3.
//  Copyright © 2015年 JiangZhiping. All rights reserved.
//

#import "OpenCVProcessingBridge.h"


@implementation OpenCVProcessingBridge {
    TickTockTimer  * processingTimer;
}

- (instancetype) init {
    self = [super init];
    if (self) {
        // Gives an default process route.
        self.cvDelegates = [[NSMutableArray alloc] init];
        self.delegateDependencies = [[NSMutableArray alloc] init];
        processingTimer = [[TickTockTimer alloc] initWithName:@"Pipeline Core" AverageLength:50];
    }
    return self;
}

+ (instancetype) getInstance {
    static OpenCVProcessingBridge * bridge;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        bridge = [[OpenCVProcessingBridge alloc] init];
        [NSObject letObject:self accessibleByName:@"OpenCVProcessingBridge"];
    });
    return bridge;
}

- (void) addCVDelegate:(id<OpenCVProcessDelegate>)aCVDelegate {
    id<OpenCVProcessDelegate> lastOne = self.cvDelegates.lastObject;
    if (lastOne) {
        [self addCVDelegate:aCVDelegate withDependency:lastOne];
    } else {
        [self addCVDelegate:aCVDelegate withDependency:aCVDelegate];
    }
}

- (void)addCVDelegate:(id<OpenCVProcessDelegate>)aCVDelegate withDependency:(id<OpenCVProcessDelegate>)dependentDelegate {
    [self.cvDelegates addObject:aCVDelegate];
    [self.delegateDependencies addObject: dependentDelegate];
}

- (CIImage *) process:(CVPixelBufferRef)pixelBuffer {
    static cv::Mat resultBGRAMat;
    resultBGRAMat = [self parallelProcessWithCvtColor:pixelBuffer];
    CIImage * resultCIImage = [CIImage imageWithCvMat:resultBGRAMat];
    return resultCIImage;
}

- (cv::Mat &) parallelProcessWithCvtColor:(CVPixelBufferRef)pixelBuffer {
    static cv::Mat bgraImage,bgrImage,resultMat,resultBGRAMat;
    static NSOperationQueue * queue = [NSOperationQueue concurrentQueueNamed:@"ProcessingQueue"];
    static NSOperationQueue * drawQueue = [NSOperationQueue serialQueueNamed:@"DrawQueue"];
    
    bgraImage = [CIImage conversionCvMatFromCVPixelBuffer:pixelBuffer];
    // remove the alpha channel
    cv::cvtColor(bgraImage, bgrImage, CV_BGRA2BGR);
    // create result preview image
    bgrImage.copyTo(resultMat);
    
    NSMutableArray<NSOperation *> *operations = [NSMutableArray new];
    for (int i = 0; i < self.cvDelegates.count; i++) {
        NSOperation * operation = [NSBlockOperation blockOperationWithBlock:^{
            self.cvDelegates[i].frameCount ++;
            self.cvDelegates[i].pixelBuffer = pixelBuffer;
            self.cvDelegates[i].inputMat = bgrImage;
            self.cvDelegates[i].resultMat = resultMat;
            [self.cvDelegates[i] process];
            
            if (self.cvDelegates[i].drawResult) {
                [drawQueue addOperationWithBlock:^{
                    [self.cvDelegates[i] drawOnResultImage];
                }];
            }
        }];
        
        int dependencyIndex = [self.cvDelegates indexOfObject: self.delegateDependencies[i]];
        if (dependencyIndex != i) {
            [operation addDependency:operations[dependencyIndex]];
        }
        
        [operations addObject:operation];
    }

    [queue addOperations:operations waitUntilFinished:YES];
    [drawQueue waitUntilAllOperationsAreFinished];
    
    // convert result mat to pixelbuffer
    cv::cvtColor(resultMat, resultBGRAMat, CV_BGR2BGRA);
    
    return resultBGRAMat;
}

@end
