//
//  TaskProcessingPipelineCore.cpp
//  OpenCVCameraPipeline
//
//  Created by JiangZhping on 2017/2/1.
//  Copyright © 2017年 JiangZhiping. All rights reserved.
//

#include "TaskProcessingPipelineCore.h"
#include <cstdio>
#include <vector>
#include <chrono>
#include <thread>
#include "tbb/flow_graph.h"
#include "tbb/tbb_allocator.h" 
#include "tbb/atomic.h"
#include "tbb/concurrent_vector.h"
using namespace tbb::flow;

std::chrono::milliseconds onemiliseconds(1);
std::chrono::milliseconds zeromiliseconds(0);
graph singlePipelineGraph;
broadcast_node< std::vector<cv::Mat> > singlePipelineStart(singlePipelineGraph);
function_node<std::vector<cv::Mat>, std::vector<cv::Mat>> * singlePipelineTail = nullptr;
int singlePipelineTaskCount = 0;
cv::Mat singlePipelineResult;
tbb::atomic<int> singlePipelineWorkingCount;
struct SinglePipelineBody {
	OpencvProcessTask * task;
	int order;
	SinglePipelineBody(OpencvProcessTask * name) : task(name) { order = singlePipelineTaskCount++; }
	std::vector<cv::Mat> operator()(std::vector<cv::Mat> input) const {
		task->frameCount++;
		task->inputMat = input[0];
		task->canvasMat = input[1];
		task->process();
		if (task->drawResult) {
			task->drawOnResultImage();
		}	
		if (order == singlePipelineTaskCount - 1) {
			singlePipelineResult = task->canvasMat;
			singlePipelineWorkingCount--;
		}
		return input;
	}
};
graph dualPipelineGraph;
broadcast_node< std::vector<cv::Mat> > dualPipelineStart(dualPipelineGraph);
function_node<std::vector<cv::Mat>, std::vector<cv::Mat>> * dualPipelineTail=nullptr;
function_node<tuple<std::vector<cv::Mat>, continue_msg>,continue_msg> * dualPipelineDrawNodeTail = nullptr;
function_node<std::vector<cv::Mat>, continue_msg> * dualPipelineFirstDrawNode = nullptr;
int dualPipelineTaskCount = 0;
cv::Mat dualPipelineResult;
tbb::atomic<int> dualPipelineWoringCount;
tbb::concurrent_vector<bool, tbb::zero_allocator<bool>> dualPipelineDrawingFlag(0);
struct DualPipelineMainBody {
	OpencvProcessTask * task;
	int order;
	DualPipelineMainBody(OpencvProcessTask * name) : task(name) { order = dualPipelineTaskCount++; }
	std::vector<cv::Mat> operator()(std::vector<cv::Mat> input) const {
		while (dualPipelineDrawingFlag[order]) {
			std::this_thread::sleep_for(zeromiliseconds);
		};
		task->frameCount++;
		task->inputMat = input[0];
		task->canvasMat = input[1];
		task->process();
		dualPipelineDrawingFlag[order] = true;
		return input;
	}
};
struct DualPipelineDrawBody
{
	OpencvProcessTask * task;
	int order;
	DualPipelineDrawBody(DualPipelineMainBody & father) : task(father.task),order(father.order) {}
	continue_msg operator()(tuple<std::vector<cv::Mat>, continue_msg>) {
		if (task->drawResult) {
			task->drawOnResultImage();
		}
		if (order == dualPipelineTaskCount - 1) {

			dualPipelineResult = task->canvasMat;
			dualPipelineWoringCount--;
		}
		dualPipelineDrawingFlag[order] = false;

		return continue_msg();
	}
};
struct DualPipelineFirstDrawBody
{
	OpencvProcessTask * task;
	int order;
	DualPipelineFirstDrawBody(DualPipelineMainBody & father) :task(father.task), order(father.order) {}
	continue_msg operator()(std::vector<cv::Mat>) {
		if (task->drawResult) {
			task->drawOnResultImage();
		}
		if (order == dualPipelineTaskCount - 1) {
			dualPipelineResult = task->canvasMat;
			dualPipelineWoringCount--;
		}
		dualPipelineDrawingFlag[order] = false;
		return continue_msg();
	}
};

void addSinglePipelineProcessingTask(OpencvProcessTask & task) {
	auto a = new function_node<std::vector<cv::Mat>, std::vector<cv::Mat>>(singlePipelineGraph, serial, SinglePipelineBody(&task));
	if (singlePipelineTail == nullptr) {
		make_edge(singlePipelineStart, *a);
		singlePipelineTail = a;
	}
	else {
		make_edge(*singlePipelineTail, *a);
		singlePipelineTail = a;
	}
}


cv::Mat parallelProcessWithSinglePipeline(const cv::Mat & rawInput) {

	std::vector<cv::Mat> input(2);
	rawInput.copyTo(input[0]);
	rawInput.copyTo(input[1]);
	while (singlePipelineWorkingCount >= 2) {
		std::this_thread::sleep_for(onemiliseconds);
	};
	singlePipelineStart.try_put(input);
	singlePipelineWorkingCount++;
	//g_onlypipe.wait_for_all();
	if (singlePipelineResult.cols > 0) {
		return singlePipelineResult;
	}
	return rawInput;
}

// Wei Zhong, come on! rewrite the above process function into tbb-parallelized version.
cv::Mat parallelProcessWithDualPipeline(const cv::Mat & rawInput) {

	cv::Mat canvasMat; rawInput.copyTo(canvasMat);
	std::vector<cv::Mat> input(2);
	rawInput.copyTo(input[0]);
	rawInput.copyTo(input[1]);
	while (dualPipelineWoringCount >= 2) {
		std::this_thread::sleep_for(onemiliseconds);
	};
	dualPipelineStart.try_put(input);
	dualPipelineWoringCount++;
	//g.wait_for_all();
	if (dualPipelineResult.cols > 0) {
		return dualPipelineResult;
	}
	return rawInput;
}


void TaskPipelineCore::addProcessingTask(OpencvProcessTask & task) {
    tasks.push_back(std::unique_ptr<OpencvProcessTask>(&task));
	addSinglePipelineProcessingTask(task);
	DualPipelineMainBody runbody(&task);
	auto a = new function_node<std::vector<cv::Mat>, std::vector<cv::Mat>>(dualPipelineGraph, serial, runbody);
	if (dualPipelineTail == nullptr) {
		make_edge(dualPipelineStart, *a);
		dualPipelineTail = a;
		dualPipelineFirstDrawNode = new function_node<std::vector<cv::Mat>, continue_msg>(dualPipelineGraph, serial, DualPipelineFirstDrawBody(runbody));
		make_edge(*dualPipelineTail, *dualPipelineFirstDrawNode);
	}
	else {
		make_edge(*dualPipelineTail, *a);
		dualPipelineTail = a;
		if (dualPipelineDrawNodeTail == nullptr) {
			auto join = new join_node<tuple<std::vector<cv::Mat>, continue_msg>>(dualPipelineGraph);
			make_edge(*dualPipelineTail, input_port<0>(*join));
			make_edge(*dualPipelineFirstDrawNode, input_port<1>(*join));
			dualPipelineDrawNodeTail = new function_node<tuple<std::vector<cv::Mat>, continue_msg>, continue_msg>(dualPipelineGraph, serial, DualPipelineDrawBody(runbody));
			make_edge(*join, *dualPipelineDrawNodeTail);
		}
		else {
			auto join = new join_node<tuple<std::vector<cv::Mat>, continue_msg>>(dualPipelineGraph);
			make_edge(*dualPipelineTail, input_port<0>(*join));
			make_edge(*dualPipelineDrawNodeTail, input_port<1>(*join));
			auto newdrawtail= new function_node<tuple<std::vector<cv::Mat>, continue_msg>, continue_msg>(dualPipelineGraph, serial, DualPipelineDrawBody(runbody));
			make_edge(*join, *newdrawtail);
			dualPipelineDrawNodeTail = newdrawtail;
		}
	}
	dualPipelineDrawingFlag.push_back(false);
}

cv::Mat TaskPipelineCore::process(const cv::Mat & rawInput, ParallizeStrategy s) {
	switch (s)
	{
	case DualPipeline:
		return parallelProcessWithDualPipeline(rawInput);
		break;
	case SinglePipeline:
		return parallelProcessWithSinglePipeline(rawInput);
		break;
	case Serial:
		break;
	default:
		break;
	}
	cv::Mat canvasMat; rawInput.copyTo(canvasMat);
    for(const std::unique_ptr<OpencvProcessTask> & ptr : tasks) {
        ptr->frameCount++;
        ptr->inputMat = rawInput;
        ptr->canvasMat = canvasMat;
        ptr->process();
        if (ptr->drawResult) {
            ptr->drawOnResultImage();
        }
    }
    return canvasMat;
}
