//
//  OpencvCameraPipeline.h
//  OpencvCameraPipeline
//
//  Created by JiangZhiping on 15/11/26.
//  Copyright © 2015年 JiangZhiping. All rights reserved.
//
#import <Foundation/Foundation.h>
#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#else
#import <AppKit/AppKit.h>
#endif
#import <CocoaCVUtilities/CocoaCVUtilities.h>

#import <OpencvCameraPipeline/OpenCVProcessingBridge.h>
#import <OpencvCameraPipeline/AbstractProcessDelegate.h>
#import <OpencvCameraPipeline/OpencvProcessPipeline.h>
#import <OpencvCameraPipeline/OpencvProcessTask.h>

#import <OpencvCameraPipeline/CameraProcessInput.h>
#import <OpencvCameraPipeline/VideoProcessInput.h>


#if TARGET_OS_IPHONE
#import <OpencvCameraPipeline/CameraPipelineViewController.h>
#import <OpencvCameraPipeline/CameraProcessInputWithSettings.h>
#import <OpencvCameraPipeline/SettingsPortalTableViewController.h>
#endif



