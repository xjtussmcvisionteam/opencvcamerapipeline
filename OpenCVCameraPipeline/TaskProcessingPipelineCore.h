//
//  TaskProcessingPipelineCore.hpp
//  OpenCVCameraPipeline
//
//  Created by JiangZhping on 2017/2/1.
//  Copyright © 2017年 JiangZhiping. All rights reserved.
//

#ifndef TaskProcessingPipelineCore_hpp
#define TaskProcessingPipelineCore_hpp

#include <opencv2/opencv.hpp>
#include <CoreOpenCVExtensions/CoreOpenCVExtensions.h>
#include "OpencvProcessTask.h"
#include <memory>

enum ParallizeStrategy {DualPipeline, SinglePipeline, Serial };
class TaskPipelineCore {
private:
    std::vector<std::unique_ptr<OpencvProcessTask>> tasks;
public:
	void addProcessingTask(OpencvProcessTask & task);
    
    cv::Mat process(const cv::Mat & rawInput, ParallizeStrategy s= DualPipeline);
};

typedef Singleton<TaskPipelineCore> TaskPipelineCoreSingleton;

#endif /* TaskProcessingPipelineCore_hpp */
