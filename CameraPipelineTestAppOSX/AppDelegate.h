//
//  AppDelegate.h
//  CameraPipelineTestAppOSX
//
//  Created by JiangZhiping on 16/3/24.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

