//
//  CameraProcessInputWithSettings.h
//  OpenCVCameraPipeline
//
//  Created by JiangZhiping on 16/4/25.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "CameraProcessInput.h"
#import <CocoaCVUtilities/CocoaCVUtilities.h>
#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#endif

@interface CameraProcessInputWithSettings : CameraProcessInput

@property (nonatomic) AVCaptureDevicePosition cameraPosition;
@property (nonatomic) AVCaptureFocusMode focusMode;
@property (nonatomic) AVCaptureExposureMode exposureMode;
@property (nonatomic) AVCaptureWhiteBalanceMode whiteBalanceMode;

#if TARGET_OS_IPHONE
@property (nonatomic) AVCaptureAutoFocusRangeRestriction autoFocusRangeRestriction;
@property (nonatomic,getter=isSmoothAutoFocusEnabled) BOOL smoothAutoFocusEnabled;
@property (nonatomic,getter=isTapToFocusEnabled) BOOL  tapToFocusEnabled;
@property (nonatomic) float lensPosition;
@property (nonatomic) float exposureDuration;
@property (nonatomic) float exposureISO;
@property (nonatomic) float whiteBalanceTemperature;
@property (nonatomic) float whiteBalanceTint;

- (void) tapToFocusToPoint: (CGPoint) devicePoint;

#endif

@end
