//
//  AbstractProcessInput.h
//  OpenCVCameraPipeline
//
//  Created by JiangZhiping on 16/4/25.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreVideo/CoreVideo.h>
#import <CoreMedia/CoreMedia.h>

@protocol ProcessInputDelegate <NSObject>

- (void) newPixelBufferDidReceive:(CVPixelBufferRef)aPixelBufferRef AtTime:(CMTime)time FrameCount:(NSUInteger)count;

@end

@protocol AbstractProcessInput <NSObject>

@required
@property (nonatomic) id<ProcessInputDelegate> delegate;

- (void) startRunning;

@optional
- (void) stopRunning;

@end
