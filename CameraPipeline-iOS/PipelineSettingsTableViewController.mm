//
//  PipelineSettingsTableViewController.m
//  GazeEstimationOnCocoa
//
//  Created by JiangZhiping on 15/11/21.
//  Copyright © 2015年 JiangZhiping. All rights reserved.
//

#import "PipelineSettingsTableViewController.h"

@interface PipelineSettingsTableViewController ()

@end

@implementation PipelineSettingsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void) setup {
    

    // output format
    _outputFormats = @[@(OpencvProcessBridgeOutputFormatCIImage), @(OpencvProcessBridgeOutputFormatCGImageRef), @(OpencvProcessBridgeOutputFormatCVPixelBufferRef)];
    _outputFormatSC.selectedSegmentIndex = [ self.outputFormats indexOfObject:@(_pipeline.processBridgeOutputFormat)];
    
    // previewer method
    _previewMethods = @[@(PipelineRenderingCGLayer), @(PipelineRenderingGLKView), @(PipelineRenderingMetal)];
    _previewerMethodSC.selectedSegmentIndex = [self.previewMethods indexOfObject:@(_pipeline.renderingMethod)];
    
}

- (IBAction)changeOutputFormat:(id)sender {
_pipeline.processBridgeOutputFormat = (OpencvProcessBridgeOutputFormat)[[self.outputFormats objectAtIndex:_outputFormatSC.selectedSegmentIndex] shortValue];
}

- (IBAction)changePreviewerMethod:(id)sender {
    _pipeline.renderingMethod = (PipelineRenderingMethod)[[self.previewMethods objectAtIndex:_previewerMethodSC.selectedSegmentIndex] shortValue];
}


@end
