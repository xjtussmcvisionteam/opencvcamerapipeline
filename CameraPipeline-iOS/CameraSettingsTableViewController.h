//
//  CameraSettingsTableViewController.h
//  GazeEstimationOnCocoa
//
//  Created by JiangZhiping on 15/11/21.
//  Copyright © 2015年 JiangZhiping. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CameraProcessInputWithSettings.h"

@interface CameraSettingsTableViewController : UITableViewController

#pragma mark - generals

@property (weak,nonatomic) CameraProcessInputWithSettings * pipelineWithSettings;


@property (nonatomic) NSArray * cameraPositions;
@property (nonatomic) NSArray * focusModes;
@property (nonatomic) NSArray * focusRanges;
@property (nonatomic) NSArray * exposureModes;
@property (nonatomic) NSArray * whiteBalanceModes;


# pragma mark - Camera Settings

@property (weak, nonatomic) IBOutlet UISwitch *discardLateFramePolicySwitch;
@property (weak, nonatomic) IBOutlet UISegmentedControl *cameraPositionSC;
@property (weak, nonatomic) IBOutlet UISegmentedControl *focusModeSC;
@property (weak, nonatomic) IBOutlet UISlider *lensPositionSlider;
@property (weak, nonatomic) IBOutlet UILabel *lensPositionLabel;
@property (weak, nonatomic) IBOutlet UISwitch *smoothedAFSwitch;

@property (weak, nonatomic) IBOutlet UISegmentedControl *exposureModeSC;
@property (weak, nonatomic) IBOutlet UISlider *durationSlider;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UISlider *isoSlider;
@property (weak, nonatomic) IBOutlet UILabel *isoLabel;

@property (weak, nonatomic) IBOutlet UISegmentedControl *whiteBalanceModeSC;
@property (weak, nonatomic) IBOutlet UISlider *whiteBalanceTemperatureSlider;
@property (weak, nonatomic) IBOutlet UILabel *whiteBalanceTemperatureLabel;
@property (weak, nonatomic) IBOutlet UISlider *whiteBalanceTintSlider;
@property (weak, nonatomic) IBOutlet UILabel *whiteBalanceTintLabel;

- (IBAction)changeDiscardLateFramePolicy:(id)sender;

- (IBAction)changeCameraPosition:(id)sender;

- (IBAction)changeFocusMode:(id)sender;

- (IBAction)changeLensPosition:(id)sender;

- (IBAction)changeSmoothedAF:(id)sender;

- (IBAction)changeExposureMode:(id)sender;

- (IBAction)changeDuration:(id)sender;

- (IBAction)changeISO:(id)sender;

- (IBAction)changeWhiteBalanceMode:(id)sender;

- (IBAction)changeWhiteBalanceTemperature:(id)sender;

- (IBAction)changeWhiteBalanceTint:(id)sender;



@end
